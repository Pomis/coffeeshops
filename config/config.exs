# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :vedro_server,
  ecto_repos: [VedroServer.Repo]

# Configures the endpoint
config :vedro_server, VedroServer.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "/qPr40LRwV8h3U/aO8H2BKgoUY6Tr8SBKANQfZVR12V1MfmeHNFnp6yBORk+8MBg",
  render_errors: [view: VedroServer.ErrorView, accepts: ~w(html json)],
  pubsub: [name: VedroServer.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
