defmodule VedroServer.ShopTest do
  use VedroServer.ModelCase

  alias VedroServer.Shop

  @valid_attrs %{average_bill: 42, description: "some description", name: "some name"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Shop.changeset(%Shop{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Shop.changeset(%Shop{}, @invalid_attrs)
    refute changeset.valid?
  end
end
