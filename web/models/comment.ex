defmodule VedroServer.Comment do
  use VedroServer.Web, :model

  @derive {Poison.Encoder, only: [:id, :text, :updated_at]}
  schema "comment" do
    field :text, :string
    field :shop_id, :integer

    timestamps()
  end

  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:integer, :shop_id])
    |> validate_required([:integer, :shop_id])
  end
end
