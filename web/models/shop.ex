defmodule VedroServer.Shop do
  use VedroServer.Web, :model

  @derive {Poison.Encoder, only: [:id, :name, :description, :average_bill, :updated_at]}
  schema "shop" do
    field :name, :string
    field :description, :string
    field :average_bill, :integer

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :description, :average_bill])
    |> validate_required([:name, :description, :average_bill])
  end
end
