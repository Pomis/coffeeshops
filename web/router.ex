defmodule VedroServer.Router do
  use VedroServer.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    # plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", VedroServer do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index

    get "/coffeeshops", CoffeeshopController, :index

    get "/coffeeshops/comments/:id", CoffeeshopController, :comments

    post "/coffeeshops/comments/:id", CoffeeshopController, :add_comment
  end

  # Other scopes may use custom stacks.
  # scope "/api", VedroServer do
  #   pipe_through :api
  # end
end
