defmodule VedroServer.PageController do
  use VedroServer.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
