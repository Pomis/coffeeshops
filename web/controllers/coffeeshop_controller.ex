defmodule VedroServer.CoffeeshopController do
  use VedroServer.Web, :controller

  def index(conn, _params) do
    json conn, (VedroServer.Shop |> Repo.all)
  end

  def comments(conn, %{"id" => id}) do
    json conn, (VedroServer.Comment |> Ecto.Query.where(shop_id: ^id) |> Repo.all)
  end

  def add_comment(conn, %{"id" => id, "text" => text}) do
      case Integer.parse(id) do
        {id, ""} when id > 0 ->
          comment = %VedroServer.Comment{shop_id: id, text: text}
          VedroServer.Repo.insert comment
          json conn, comment
        _ ->
          json conn, 400
  end
    
  end

end
