defmodule VedroServer.Repo.Migrations.CreateComment do
  use Ecto.Migration

  def change do
    create table(:comment) do
      add :text, :string
      add :shop_id, :integer
      timestamps()
    end
  end
end
