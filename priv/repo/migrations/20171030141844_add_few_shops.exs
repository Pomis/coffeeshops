defmodule VedroServer.Repo.Migrations.AddFewShops do
  use Ecto.Migration

  def change do
    shops = [
      %VedroServer.Shop{name: "Holymosh Cannabis Club", description: "Only best sativa in Europe", average_bill: 15},
      %VedroServer.Shop{name: "American mix", description: "In this coffeeshop you can mix everything you want. You can pay both in euros and dollars.", average_bill: 10},
      %VedroServer.Shop{name: "Preeton", description: "League of Bongo", average_bill: 40},
      %VedroServer.Shop{name: "Gornjak-2", description: "Sportclub with weed retirement sessions", average_bill: 10}
    ]
    shops |> Enum.each(fn x -> VedroServer.Repo.insert x end)
  end
end
