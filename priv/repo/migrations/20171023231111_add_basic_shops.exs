defmodule VedroServer.Repo.Migrations.AddBasicShops do
  use Ecto.Migration

  def change do
    # shop1 = %VedroServer.Shop{}
    # %{shop1 | name: "G13 Club"}
    shops = [
      %VedroServer.Shop{name: "G13 Club", description: "Best cannabis club in Barcelona", average_bill: 16},
      %VedroServer.Shop{name: "Weed sheet", description: "Only there you can meet Smoky Mo", average_bill: 14},
      %VedroServer.Shop{name: "High Lounge", description: "Best secret place in Moscow", average_bill: 24}
    ]
    shops |> Enum.each(fn x -> VedroServer.Repo.insert x end)
  end
end
