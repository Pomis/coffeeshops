defmodule VedroServer.Repo.Migrations.CreateShop do
  use Ecto.Migration
  use VedroServer.Web, :model

  def change do
    create table(:shop) do
      add :name, :string
      add :description, :string
      add :average_bill, :integer

      timestamps()
    end
  end
end
