defmodule VedroServer.Repo.Migrations.AddBasicComments do
  use Ecto.Migration

  def change do
    comments = [
      %VedroServer.Comment{text: "Awesome place!!!11 Luv it so much", shop_id: 1},
      %VedroServer.Comment{text: "Not bad at all", shop_id: 2}
    ]
    comments |> Enum.each(fn x -> VedroServer.Repo.insert x end)
  end
end
