# VedroServer
  
Методы API
  
Получение списка всех кофешопов    
`GET   /coffeeshops`               
  
Получение комментариев по определённому кофешопу    
`GET   /coffeeshops/comments/:id`
  
Оставить комментарий.   
`POST  /coffeeshops/comments/:id`  
Body Параметры запроса:  
`text: string`        Текст комментария   